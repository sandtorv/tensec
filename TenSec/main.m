//
//  main.m
//  TenSec
//
//  Created by Sebastian Sandtorv on 13.01.14.
//  Copyright (c) 2014 IxDNor. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
