//
//  MenuScene.m
//  TenSec
//
//  Created by Sebastian Sandtorv on 15.01.14.
//  Copyright (c) 2014 IxDNor. All rights reserved.
//

#import "MenuScene.h"
#import "MyScene.h"
#import "StatScene.h"
#import "ViewController.h"


@interface MenuScene(){
    
}

@end

@implementation MenuScene

- (instancetype)initWithSize:(CGSize)size
{
    if(self = [super initWithSize:size]) {
        
        SKSpriteNode *background = [SKSpriteNode spriteNodeWithColor:[SKColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0] size:CGSizeMake(CGRectGetMaxX(self.frame), CGRectGetMaxY(self.frame))];
        background.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
        [self addChild:background];
        
        SKLabelNode *title = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-UltraLight"];
        title.text = @"TenSec";
        title.fontSize = 60;
        title.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMaxY(self.frame) - 150);
        title.fontColor = [SKColor blackColor];
        
        [self addChild:title];
        
        SKLabelNode *tapToPlay = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-UltraLight"];
        tapToPlay.text = @"Play";
        tapToPlay.name = @"tapToPlay";
        tapToPlay.fontSize = 80;
        tapToPlay.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame)-50);
        tapToPlay.fontColor = [SKColor colorWithRed:0 green:0.6 blue:1 alpha:1.0];
        [self addChild:tapToPlay];
        
        SKLabelNode *stats = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-Light"];
        stats.text = @"Stats";
        stats.name = @"statsButton";
        stats.fontSize = 20;
        stats.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMinY(self.frame)+120);
        stats.fontColor = [SKColor colorWithRed:0 green:0.6 blue:1 alpha:1.0];
        [self addChild:stats];
        
        SKLabelNode *lastGame = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-Light"];
        NSInteger currentScoreInt = [[NSUserDefaults standardUserDefaults] integerForKey: @"currentScore"];
        NSString *scoreString = [NSString stringWithFormat:@"You clicked %ld last game", (long)currentScoreInt];
        lastGame.text = scoreString;
        lastGame.name = @"statsButton";
        lastGame.fontSize = 16;
        lastGame.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMinY(self.frame)+100);
        lastGame.fontColor = [SKColor blackColor];
        [self addChild:lastGame];
        
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //check if they touched our Restart Label
    for (UITouch *touch in touches) {
        SKNode *n = [self nodeAtPoint:[touch locationInNode:self]];
        if (n != self && [n.name isEqual: @"tapToPlay"]) {
            MyScene *game = [[MyScene alloc] initWithSize:self.size];
            [self.view presentScene:game];
            return;
        }
        if (n != self && [n.name isEqual: @"statsButton"]) {
            StatScene *stats = [[StatScene alloc] initWithSize:self.size];
            [self.view presentScene:stats];
            return;
        }
    }
}

@end