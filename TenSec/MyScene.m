//
//  MyScene.m
//  TenSec
//
//  Created by Sebastian Sandtorv on 13.01.14.
//  Copyright (c) 2014 IxDNor. All rights reserved.
//

#import "MyScene.h"
#import "MenuScene.h"
#import "StatScene.h"
#import "ViewController.h"


@implementation MyScene{
    NSMutableArray *_clicks;
    BOOL _dead;
    NSInteger countDown;
    NSInteger scoreCounter;
    SKLabelNode *scoreText;
    SKLabelNode *timerText;
    SKLabelNode *restartButton;
    SKLabelNode *countDownText;
}

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        self.backgroundColor = [SKColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
        _clicks = [NSMutableArray new];
        scoreCounter = 0;
        
        scoreText = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-UltraLight"];
        scoreText.text = @"";
        scoreText.fontSize = 20;
        scoreText.fontColor = [SKColor colorWithRed:0.05 green:0.05 blue:0.05 alpha:1.0];
        scoreText.position = CGPointMake(CGRectGetMinX(self.frame)+60,CGRectGetMinY(self.frame)+10);
        [self addChild:scoreText];
        
        countDownText = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-UltraLight"];
        countDownText.text = @"";
        countDownText.fontSize = 80;
        countDownText.fontColor = [SKColor colorWithRed:0.05 green:0.05 blue:0.05 alpha:1.0];
        countDownText.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
        [self addChild:countDownText];
        SKAction *zoomIn = [SKAction scaleTo:1.1 duration:0.5];
        SKAction *zoomOut = [SKAction scaleTo:0.9 duration:0.5];
        SKAction *pop = [SKAction sequence:@[zoomIn,zoomOut]];
        SKAction *popAnimation = [SKAction repeatActionForever:pop];
        [countDownText runAction:popAnimation];
        
        timerText = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-UltraLight"];
        timerText.text = @"";
        timerText.fontSize = 20;
        timerText.fontColor = [SKColor colorWithRed:0.05 green:0.05 blue:0.05 alpha:1.0];
        timerText.position = CGPointMake(CGRectGetMaxX(self.frame)-60,CGRectGetMinY(self.frame)+10);
        [self addChild:timerText];
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    for (UITouch *touch in touches){
        SKNode *n = [self nodeAtPoint:[touch locationInNode:self]];
        if (n != self && [n.name isEqual: @"restart"]) {
            NSLog(@"restart");
            
            return;
        }
        if(!_dead && countDown <= 780 && countDown >= 180){
            CGPoint location = [touch locationInNode:self];
            SKNode *clickObject = [SKNode node];
            SKLabelNode *sprite = [SKLabelNode labelNodeWithFontNamed:@"Helvetica-Neue"];
            sprite.text = @"+1";
            sprite.fontColor = [SKColor colorWithRed:0.05 green:0.05 blue:0.05 alpha:1.0];
            sprite.fontSize = 30 + arc4random() % (100 - 30);
            sprite.position = location;
            SKAction *move = [SKAction moveTo:CGPointMake(CGRectGetMinX(self.frame)+60,CGRectGetMinY(self.frame)+10) duration:0.5];
            SKAction *remove = [SKAction fadeOutWithDuration:0.5];
            SKAction *group = [SKAction group:@[move, remove]];
            [sprite runAction:[SKAction repeatAction:group count:1]];
            [clickObject addChild:sprite];
            [_clicks addObject:clickObject];
            [self addChild:clickObject];
            scoreCounter++;
        }
    }
}

-(void)update:(CFTimeInterval)currentTime {
    if (!_dead && countDown <= 780) {
        if(countDown >= 180){
            timerText.text = [NSString stringWithFormat:@"%ld s left",(long)((countDown-840)/60)*-1];
            countDownText.text = @"";
        }
        if(countDown >= 180 && countDown <= 780)scoreText.text = [NSString stringWithFormat:@"%ld clicks",(long)scoreCounter];
        if(countDown > 180 && scoreCounter <= 0)countDownText.text = @"CLICK!";
        if(countDown < 180)countDownText.text = [NSString stringWithFormat:@"%ld",(long)(((countDown-240)/60)*-1)];
        countDown++;
        NSLog(@"%ld", (long)countDown);
    }
    else{
        // Highscore
        if(scoreCounter > [[NSUserDefaults standardUserDefaults] integerForKey: @"highScore"]){
            NSUserDefaults *highScore = [NSUserDefaults standardUserDefaults];
            [highScore setInteger:scoreCounter forKey:@"highScore"];
            [highScore synchronize];
            [self reportHighScore:scoreCounter];
        }
        // Latest game score
        NSUserDefaults *currentScore = [NSUserDefaults standardUserDefaults];
        [currentScore setInteger:scoreCounter forKey:@"currentScore"];
        [currentScore synchronize];
        
        
        // Total game score
        NSUserDefaults *totalScore = [NSUserDefaults standardUserDefaults];
        [totalScore setInteger:(scoreCounter+[[NSUserDefaults standardUserDefaults] integerForKey: @"totalScore"]) forKey:@"totalScore"];
        [totalScore synchronize];
        [self reportTotalScore:[[NSUserDefaults standardUserDefaults] integerForKey: @"totalScore"]];
        
        _dead = true;
        
        MenuScene *menu = [[MenuScene alloc] initWithSize:self.size];
        [self.view presentScene:menu];
    }
}

- (void) reportHighScore:(NSInteger) highScore {
    NSLog(@"High score GC");
    if ([GKLocalPlayer localPlayer].isAuthenticated) {
        GKScore* score = [[GKScore alloc] initWithLeaderboardIdentifier:@"grp.com.ixdnor.TenSecHighScore"];
        score.value = highScore;
        [GKScore reportScores:@[score] withCompletionHandler:^(NSError *error) {
            if (error) {
                NSLog(@"%@",error);
            }
        }];
    }
}

- (void) reportTotalScore:(NSInteger) highScore {
    NSLog(@"Total score GC");
    if ([GKLocalPlayer localPlayer].isAuthenticated) {
        GKScore* score = [[GKScore alloc] initWithLeaderboardIdentifier:@"grp.com.ixdnor.TenSecTotal"];
        score.value = highScore;
        [GKScore reportScores:@[score] withCompletionHandler:^(NSError *error) {
            if (error) {
                NSLog(@"%@",error);
            }
        }];
    }
}
@end
