//
//  AppDelegate.h
//  TenSec
//
//  Created by Sebastian Sandtorv on 13.01.14.
//  Copyright (c) 2014 IxDNor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
