//
//  ViewController.h
//  TenSec
//

//  Copyright (c) 2014 IxDNor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <GameKit/GameKit.h>
#import <iAd/iAd.h>

@interface ViewController : UIViewController <GKGameCenterControllerDelegate, ADBannerViewDelegate>{
    
}
-(void)showGameCenter;
@end
