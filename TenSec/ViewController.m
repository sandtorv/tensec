//
//  ViewController.m
//  TenSec
//
//  Created by Sebastian Sandtorv on 13.01.14.
//  Copyright (c) 2014 IxDNor. All rights reserved.
//

#import "ViewController.h"
#import "MyScene.h"
#import "MenuScene.h"


@interface ViewController(){
    SKView *skView;
}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self authenticate];

    // Configure the view.
    skView = (SKView *)self.view;
    skView.showsFPS = NO;
    skView.showsNodeCount = NO;
    
    // Create and configure the scene.
    SKScene * scene = [MenuScene sceneWithSize:skView.bounds.size];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:scene];
    
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

// Game center - opens and closes the game center leaderboard and achivements in the application
// User authentication
-(void)authenticate
{
    __weak GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error)
    {
        if(viewController)
        {
            [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:viewController animated:YES completion:nil];
        }
        else if(localPlayer.isAuthenticated == YES)
        {
            [localPlayer generateIdentityVerificationSignatureWithCompletionHandler:^(NSURL *publicKeyUrl, NSData *signature, NSData *salt, uint64_t timestamp, NSError *error) {
                
                if(error != nil)
                {
                    return; //some sort of error, can't authenticate right now
                }
                
                NSLog(@"Authenticated user!");
            }];
        }
        else
        {
            NSLog(@"game center disabled");
        }
    };
}
//Game center stuff
-(void)showGameCenter{
    GKGameCenterViewController* gameCenterController = [[GKGameCenterViewController alloc] init];
    gameCenterController.viewState = GKGameCenterViewControllerStateLeaderboards;
    gameCenterController.gameCenterDelegate = self;
    [self presentViewController:gameCenterController animated:YES completion:nil];
}
- (void) gameCenterViewControllerDidFinish:(GKGameCenterViewController*) gameCenterViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}
// End game center

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}
#pragma mark iAd Delegate Methods

-(void)bannerViewDidLoadAd:(ADBannerView *)banner{
    if(![[NSUserDefaults standardUserDefaults] integerForKey: @"iad"] == 1){
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1];
        [banner setAlpha:1];
        [UIView commitAnimations];
    }
}

-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error{
    if(![[NSUserDefaults standardUserDefaults] integerForKey: @"iad"] == 1){
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1];
        [banner setAlpha:0];
        [UIView commitAnimations];
    }
}

@end
