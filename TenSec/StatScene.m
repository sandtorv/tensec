//
//  StatScene.m
//  TenSec
//
//  Created by Sebastian Sandtorv on 15.01.14.
//  Copyright (c) 2014 IxDNor. All rights reserved.
//

#import "StatScene.h"
#import "MenuScene.h"
#import "MyScene.h"
#import "ViewController.h"

@interface StatScene(){
    
}

@end

@implementation StatScene

- (instancetype)initWithSize:(CGSize)size
{
    if(self = [super initWithSize:size]) {
        
        SKSpriteNode *background = [SKSpriteNode spriteNodeWithColor:[SKColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0] size:CGSizeMake(CGRectGetMaxX(self.frame), CGRectGetMaxY(self.frame))];
        background.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
        [self addChild:background];
        
        SKLabelNode *backButton = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-UltraLight"];
        backButton.text = @"Back";
        backButton.name = @"back";
        backButton.fontSize = 20;
        backButton.position = CGPointMake(CGRectGetMinX(self.frame)+40,CGRectGetMinY(self.frame)+20);
        backButton.fontColor = [SKColor colorWithRed:0 green:0.6 blue:1 alpha:1.0];
        [self addChild:backButton];
        
        SKLabelNode *title = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-UltraLight"];
        title.text = @"TenSec stats";
        title.fontSize = 60;
        title.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMaxY(self.frame) - 150);
        title.fontColor = [SKColor blackColor];
        [self addChild:title];
        
        SKLabelNode *GameCenter = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-UltraLight"];
        GameCenter.text = @"Show Game Center";
        GameCenter.name = @"GCButton";
        GameCenter.fontSize = 25;
        GameCenter.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame) - 100);
        GameCenter.fontColor = [SKColor colorWithRed:0 green:0.6 blue:1 alpha:1.0];
        [self addChild:GameCenter];
        
        SKLabelNode *highscore = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-Light"];
        NSInteger scoreInt = [[NSUserDefaults standardUserDefaults] integerForKey: @"highScore"];
        NSString *highscoreString = [NSString stringWithFormat:@"Your best score is %ld clicks", (long)scoreInt];
        highscore.text = highscoreString;
        highscore.fontSize = 16;
        highscore.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMinY(self.frame) + 110);
        highscore.fontColor = [SKColor blackColor];
        [self addChild:highscore];
        
        SKLabelNode *score = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-Light"];
        NSInteger currentScoreInt = [[NSUserDefaults standardUserDefaults] integerForKey: @"currentScore"];
        NSString *scoreString = [NSString stringWithFormat:@"You clicked %ld last game", (long)currentScoreInt];
        score.text = scoreString;
        score.fontSize = 16;
        score.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMinY(self.frame) + 80);
        score.fontColor = [SKColor blackColor];
        [self addChild:score];
        
        SKLabelNode *totalClicks = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-Light"];
        NSInteger totalClicksInt = [[NSUserDefaults standardUserDefaults] integerForKey: @"totalScore"];
        NSString *totalClicksString = [NSString stringWithFormat:@"Totally %ld clicks", (long)totalClicksInt];
        totalClicks.text = totalClicksString;
        totalClicks.fontSize = 16;
        totalClicks.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMinY(self.frame) + 50);
        totalClicks.fontColor = [SKColor blackColor];
        [self addChild:totalClicks];
        
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    //check if they touched our Restart Label
    for (UITouch *touch in touches) {
        SKNode *n = [self nodeAtPoint:[touch locationInNode:self]];
        if (n != self && [n.name isEqual: @"tapToPlay"]) {
            MyScene *game = [[MyScene alloc] initWithSize:self.size];
            [self.view presentScene:game];
            return;
        }
        if (n != self && [n.name isEqual: @"back"]) {
            MenuScene *menu = [[MenuScene alloc] initWithSize:self.size];
            [self.view presentScene:menu];
            return;
        }
        if (n != self && [n.name isEqual: @"GCButton"]) {
            [self showGameCenter];
            return;
        }
    }
}

-(void)showGameCenter{
    ViewController *vc = self.view.window.rootViewController;
    [vc showGameCenter];
}
@end
